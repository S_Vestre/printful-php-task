<?php
header('Access-Control-Allow-Origin: *');  
header("Content-Type: application/json");

require_once __DIR__."/../../backend/vendor/autoload.php";

use App\Database;
use App\Config;
use App\Models\TempUser;


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_POST = json_decode(file_get_contents("php://input"), true);

    $config = new Config();
    
    $db = new Database($config);
    $db = $db->getConnection();
    $tmpUsr = new TempUser($db);

    if(isset($_POST["name"], $_POST["test_id"])){
        
        $name = $_POST["name"];
        $test_id = $_POST["test_id"];
        if(strlen($name) < 3) die("Error: Name can't be shorter than 3 letters");
        if($test_id == 0) die("Error: Invalid test ID");
        
        $user = $tmpUsr->createUser($name, $test_id);
        if($user) {
            die(json_encode([
                "status" => "success",
                "user" => $user
            ]));
        } else {
            die(json_encode([
                "status" => "fail",
                "message" => "Error: No such test id found."
            ]));
        }
        
    }
    
}
