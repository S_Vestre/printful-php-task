<?php
header('Access-Control-Allow-Origin: *');  
header("Content-Type: application/json");

require_once __DIR__."/../../backend/vendor/autoload.php";

use App\Database;
use App\Config;
use App\Models\Test;

$config = new Config();

$db = new Database($config);
$db = $db->getConnection();
$test = new Test($db);

if(isset($_GET["id"])){
    $ret = [
        "text" => $test->questions->getQuestion($_GET["id"]),
        "answers" => $test->questions->Question($_GET["id"])->getAnswers()
    ];
    echo json_encode($ret);
}