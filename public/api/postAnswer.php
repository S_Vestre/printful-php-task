<?php
header('Access-Control-Allow-Origin: *');  
header("Content-Type: application/json");

require_once __DIR__."/../../backend/vendor/autoload.php";

use App\Database;
use App\Config;
use App\Models\TempUser;
use App\Models\Test;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_POST = json_decode(file_get_contents("php://input"), true);

    $config = new Config();
    
    $db = new Database($config);
    $db = $db->getConnection();
    $tmpUsr = new TempUser($db);

    if(isset($_POST["name"], $_POST["test_id"], $_POST["question_id"], $_POST["answer_id"])){
        
        extract($_POST); // Convert all array variables into local variable table

        $user_id = $tmpUsr->findUserId($name, $test_id);

        // Make a new Test instance to get answer result
        $test = new Test($db, $test_id);
        $isCorrect = $test->questions->Question($question_id)->isCorrect($answer_id);

        // Make a new TempUser instance with real user_id
        $tmpUsr = new TempUser($db, $user_id);
        // Send in an actual answer
        $tmpUsr->userAnswer->giveAnswer($test_id, $answer_id, $isCorrect);
        // Get the next question, false if there isn't any
        $next = $test->questions->getNextQuestion($question_id);
        if($next){
            $tmpUsr->setQuestionId($next);
            $tmpUsr->addToQuestion();
        } else {
            $tmpUsr->setFinished(); // finished = 1
        }

        // Send back to client
        echo json_encode([
            "answer_id" => $test->questions->Question($question_id)->getCorrectId(),
            "next_question" => $next ?: "done"
        ]);
    }
}