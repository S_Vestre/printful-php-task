-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 28, 2018 at 12:02 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `PrintfulTest`
--

-- --------------------------------------------------------

--
-- Table structure for table `Answer`
--

CREATE TABLE `Answer` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `correct` int(1) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Answer`
--

INSERT INTO `Answer` (`id`, `name`, `correct`, `question_id`) VALUES
(1, 'Macaulay Culkin', 1, 3),
(2, 'McCauley Kulkin', 0, 3),
(3, 'Charles Clukin', 0, 3),
(4, 'Clukin Culkin', 0, 3),
(5, 'None', 0, 4),
(6, 'One', 1, 4),
(7, 'Three', 0, 4),
(8, 'February 14th', 0, 5),
(9, 'July 21st', 0, 5),
(10, 'February 2nd', 1, 5),
(11, 'December 20th', 0, 5),
(12, 'November 29th', 0, 5),
(13, 'Toronto', 0, 6),
(14, 'Chicago', 1, 6),
(15, 'Pink', 0, 7),
(16, 'Green', 0, 7),
(17, 'Red', 0, 7),
(18, 'Blue', 1, 7),
(19, '1952', 0, 8),
(20, '1970', 0, 8),
(21, '1990', 0, 8),
(22, '2001', 0, 8),
(23, '1960', 0, 8),
(24, '1955', 1, 8),
(25, '1 year', 0, 9),
(26, 'It didn\'t exist yet', 0, 9),
(27, '10 years', 0, 9),
(28, '5 years', 1, 9),
(29, 'Philips', 0, 10),
(30, 'Sony', 1, 10),
(31, 'IBM', 0, 10),
(32, '10 years', 0, 11),
(33, '5 years', 0, 11),
(34, '3 years', 0, 11),
(35, '7 years', 0, 11),
(36, '15 years', 0, 11),
(37, '2 years', 0, 11),
(38, 'Not known', 0, 11),
(39, '6 years', 1, 11),
(40, 'About terrorism', 0, 12),
(41, 'About starving children', 1, 12),
(42, 'About World War 2', 0, 12),
(43, 'About collapsing buildings', 0, 12),
(44, 'Tom & Jerry', 0, 13),
(45, 'The Simpsons', 1, 13),
(46, 'Futurama', 0, 13),
(47, 'Family Guy', 0, 13),
(48, 'American Dad', 0, 13),
(49, 'Bob\'s burgers', 0, 13),
(50, '70$ million', 0, 14),
(51, '50$ million', 1, 14),
(52, '35$ million', 0, 14),
(53, 'Yes', 1, 15),
(54, 'No', 0, 15),
(55, 'In 1965', 0, 16),
(56, 'In 1972', 0, 16),
(57, 'In 1959', 1, 16),
(58, 'In 1977', 0, 16),
(59, 'In 1981', 0, 16),
(60, 'In 1950', 0, 16);

-- --------------------------------------------------------

--
-- Table structure for table `Question`
--

CREATE TABLE `Question` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `test_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Question`
--

INSERT INTO `Question` (`id`, `name`, `test_id`) VALUES
(3, 'Who plays the main role in Home Alone?', 1),
(4, 'How many oscars has Leonardo DiCaprio won?', 1),
(5, 'What date is \'Groundhog Day\'?', 1),
(6, '\'Home Alone\' is set in which city?', 1),
(7, 'What pill does Neo take in \'The Matrix\'?', 1),
(8, 'High definition television actually started in...', 2),
(9, 'For how long time did BBC went off during WW2?', 2),
(10, 'Who made the first pocket TV in 1982?', 2),
(11, 'How many years of life does the average person spends watching TV?', 2),
(12, 'What was the 1995 television documentary film called “The Dying Rooms” about?', 2),
(13, 'What is the most popular kids TV show?', 2),
(14, 'How much did the most expensive TV ad cost?', 2),
(15, 'The first television remote was called the Lazy Bone', 2),
(16, 'When was the first colored TV released?', 2);

-- --------------------------------------------------------

--
-- Table structure for table `TempUser`
--

CREATE TABLE `TempUser` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `current_question` int(11) NOT NULL,
  `max_question` int(11) NOT NULL,
  `user_ip` text NOT NULL,
  `finished` int(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TempUser`
--

INSERT INTO `TempUser` (`id`, `name`, `test_id`, `question_id`, `current_question`, `max_question`, `user_ip`, `finished`, `created_at`) VALUES
(121, 'maita', 1, 4, 2, 5, '127.0.0.1', 0, '2018-12-28 12:10:41'),
(122, 'Laiks', 1, 6, 5, 5, '127.0.0.1', 0, '2018-12-28 12:12:07'),
(123, 'Baite', 1, 7, 5, 5, '127.0.0.1', 0, '2018-12-28 12:12:59'),
(124, 'Bestfr', 1, 7, 5, 5, '127.0.0.1', 1, '2018-12-28 12:27:36'),
(125, 'Laikk', 1, 4, 2, 5, '127.0.0.1', 0, '2018-12-28 12:30:20'),
(126, 'bbddd', 1, 7, 5, 5, '127.0.0.1', 1, '2018-12-28 12:30:49'),
(127, 'asd as d', 1, 3, 1, 5, '127.0.0.1', 0, '2018-12-28 12:32:02'),
(128, 'fgdf gdf', 2, 16, 9, 9, '127.0.0.1', 1, '2018-12-28 12:32:08'),
(129, 'Bates', 1, 7, 5, 5, '127.0.0.1', 1, '2018-12-28 12:39:52'),
(130, 'Lokatron', 1, 7, 5, 5, '127.0.0.1', 1, '2018-12-28 12:41:17'),
(131, 'Norman', 1, 7, 5, 5, '127.0.0.1', 1, '2018-12-28 12:43:01'),
(132, 'Norman', 1, 7, 5, 5, '127.0.0.1', 1, '2018-12-28 12:46:02'),
(133, 'asdasd', 1, 7, 5, 5, '127.0.0.1', 1, '2018-12-28 13:06:57'),
(134, 'Rocker', 1, 7, 5, 5, '127.0.0.1', 1, '2018-12-28 13:36:10');

-- --------------------------------------------------------

--
-- Table structure for table `Test`
--

CREATE TABLE `Test` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Test`
--

INSERT INTO `Test` (`id`, `name`) VALUES
(1, 'History of movies'),
(2, 'History of television');

-- --------------------------------------------------------

--
-- Table structure for table `UserAnswer`
--

CREATE TABLE `UserAnswer` (
  `id` int(11) NOT NULL,
  `temp_user_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `correct` int(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `UserAnswer`
--

INSERT INTO `UserAnswer` (`id`, `temp_user_id`, `answer_id`, `test_id`, `correct`, `created_at`) VALUES
(72, 121, 1, 1, 1, '2018-12-28 12:10:46'),
(73, 122, 1, 1, 1, '2018-12-28 12:12:09'),
(74, 122, 6, 1, 1, '2018-12-28 12:12:17'),
(75, 122, 11, 1, 0, '2018-12-28 12:12:22'),
(76, 122, 10, 1, 1, '2018-12-28 12:12:24'),
(77, 123, 1, 1, 1, '2018-12-28 12:13:01'),
(78, 123, 5, 1, 0, '2018-12-28 12:13:08'),
(79, 123, 10, 1, 1, '2018-12-28 12:13:13'),
(80, 123, 14, 1, 1, '2018-12-28 12:13:17'),
(81, 123, 18, 1, 1, '2018-12-28 12:13:22'),
(82, 124, 2, 1, 0, '2018-12-28 12:27:38'),
(83, 124, 6, 1, 1, '2018-12-28 12:27:44'),
(84, 124, 10, 1, 1, '2018-12-28 12:27:49'),
(85, 124, 14, 1, 1, '2018-12-28 12:27:54'),
(86, 124, 16, 1, 0, '2018-12-28 12:28:01'),
(87, 125, 2, 1, 0, '2018-12-28 12:30:23'),
(88, 126, 2, 1, 0, '2018-12-28 12:30:50'),
(89, 126, 6, 1, 1, '2018-12-28 12:30:57'),
(90, 126, 11, 1, 0, '2018-12-28 12:31:01'),
(91, 126, 14, 1, 1, '2018-12-28 12:31:05'),
(92, 126, 15, 1, 0, '2018-12-28 12:31:10'),
(93, 128, 21, 2, 0, '2018-12-28 12:32:22'),
(94, 128, 26, 2, 0, '2018-12-28 12:32:29'),
(95, 128, 31, 2, 0, '2018-12-28 12:32:37'),
(96, 128, 38, 2, 0, '2018-12-28 12:32:52'),
(97, 128, 43, 2, 0, '2018-12-28 12:33:02'),
(98, 128, 45, 2, 1, '2018-12-28 12:33:09'),
(99, 128, 51, 2, 1, '2018-12-28 12:33:15'),
(100, 128, 53, 2, 1, '2018-12-28 12:33:21'),
(101, 128, 55, 2, 0, '2018-12-28 12:33:31'),
(102, 129, 1, 1, 1, '2018-12-28 12:39:54'),
(103, 129, 6, 1, 1, '2018-12-28 12:39:58'),
(104, 129, 8, 1, 0, '2018-12-28 12:40:01'),
(105, 129, 14, 1, 1, '2018-12-28 12:40:07'),
(106, 129, 17, 1, 0, '2018-12-28 12:40:11'),
(107, 130, 2, 1, 0, '2018-12-28 12:41:19'),
(108, 130, 6, 1, 1, '2018-12-28 12:41:23'),
(109, 130, 9, 1, 0, '2018-12-28 12:41:27'),
(110, 130, 14, 1, 1, '2018-12-28 12:41:32'),
(111, 130, 18, 1, 1, '2018-12-28 12:41:37'),
(112, 131, 1, 1, 1, '2018-12-28 12:43:02'),
(113, 131, 6, 1, 1, '2018-12-28 12:43:06'),
(114, 131, 8, 1, 0, '2018-12-28 12:43:10'),
(115, 131, 14, 1, 1, '2018-12-28 12:43:14'),
(116, 131, 15, 1, 0, '2018-12-28 12:43:19'),
(117, 132, 1, 1, 1, '2018-12-28 12:46:03'),
(118, 132, 6, 1, 1, '2018-12-28 12:46:07'),
(119, 132, 9, 1, 0, '2018-12-28 12:46:11'),
(120, 132, 14, 1, 1, '2018-12-28 12:46:15'),
(121, 132, 16, 1, 0, '2018-12-28 12:46:19'),
(122, 133, 1, 1, 1, '2018-12-28 13:06:58'),
(123, 133, 7, 1, 0, '2018-12-28 13:07:03'),
(124, 133, 10, 1, 1, '2018-12-28 13:07:07'),
(125, 133, 13, 1, 0, '2018-12-28 13:07:11'),
(126, 133, 18, 1, 1, '2018-12-28 13:07:15'),
(127, 134, 1, 1, 1, '2018-12-28 13:36:12'),
(128, 134, 5, 1, 0, '2018-12-28 13:36:16'),
(129, 134, 10, 1, 1, '2018-12-28 13:36:20'),
(130, 134, 14, 1, 1, '2018-12-28 13:36:25'),
(131, 134, 16, 1, 0, '2018-12-28 13:36:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Answer`
--
ALTER TABLE `Answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Question`
--
ALTER TABLE `Question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TempUser`
--
ALTER TABLE `TempUser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Test`
--
ALTER TABLE `Test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `UserAnswer`
--
ALTER TABLE `UserAnswer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Answer`
--
ALTER TABLE `Answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `Question`
--
ALTER TABLE `Question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `TempUser`
--
ALTER TABLE `TempUser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `Test`
--
ALTER TABLE `Test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `UserAnswer`
--
ALTER TABLE `UserAnswer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
