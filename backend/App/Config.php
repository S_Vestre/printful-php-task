<?php
namespace App;

class Config {

    private $db_host = "localhost";
    private $db_user = "root";
    private $db_pass = "root";
    private $db_name = "PrintfulTest";

    public function getData($data) {
        $ret = false;
        switch($data) {
            case "host": {
                $ret = $this->db_host;
                break;
            }
            case "user": {
                $ret = $this->db_user;
                break;
            }
            case "pass": {
                $ret = $this->db_pass;
                break;
            }
            case "db": {
                $ret = $this->db_name;
                break;
            }
            case "arr": {
                $ret = [
                    "host"  => $this->db_host,
                    "user"  => $this->db_user,
                    "pass"  => $this->db_pass,
                    "db"    => $this->db_name
                ];
                break;
            }
        }
        return $ret;
    }
}