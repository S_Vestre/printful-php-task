<?php
namespace App;

// Base Model class for all models who share the same methods
class BaseModel {

    protected $conn;

    public function __construct($db) {
        // Get the DB
        $this->conn = $db;
    }

    /**
     * Returns all rows of the current Model/Table
     *
     * @return void
     */
    public function all() {
        $query = "SELECT * FROM ".$this->table;
        $prp = $this->conn->prepare($query);
        $prp->execute();
        $rows = [];
        if($prp->rowCount() > 0) {
            while($row = $prp->fetch(\PDO::FETCH_ASSOC)) {
                extract($row);

                $item = [
                    'id' => $id,
                    'name' => $name
                ];
                array_push($rows, $item);
            }
        }
        return $rows;
    }

    /**
     * Inserts a new record into the DB with given data
     *
     * @param String $fields The fields in String format seperated by comma
     * @param String $data The data in the same order as fields and seperated by comma
     * @return Array if successful, False if not
     */
    public function new($fields, $data) {
        
        // Prepare the query
        $query = "INSERT INTO ".$this->table." (".$fields.") VALUES (".$data.")";

        // Try to insert new data, if successful, return the data from newly created row
        if($this->conn->prepare($query)->execute()){
            $query = "SELECT * FROM ".$this->table." WHERE id='".$this->conn->lastInsertId()."'";
            $prp = $this->conn->prepare($query);
            $prp->execute();
            $row = $prp->fetch(\PDO::FETCH_ASSOC);
            return $row;
        }
        return false;
    }

    /**
     * Shortening method for simple query
     *
     * @param String $query The query which you want to execute
     * @return Object
     */
    public function query($query) {
        $prp = $this->conn->prepare($query);
        $prp->execute();
        return $prp;
    }
}