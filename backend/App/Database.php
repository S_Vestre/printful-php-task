<?php
namespace App;

class Database {

    // Database parameters for connection
    private $config;
    private $host;
    private $user;
    private $pass;
    private $db;
    private $conn;

    public function __construct($config){
        $this->config = $config;
        /*
        $this->host = $config["db_host"];
        $this->user = $config["db_user"];
        $this->pass = $config["db_pass"];
        $this->db = $config["db_name"];*/

        $this->host = $this->config->getData("host");
        $this->user = $this->config->getData("user");
        $this->pass = $this->config->getData("pass");
        $this->db = $this->config->getData("db");

        try {
            $this->conn = new \PDO("mysql:host=".$this->host.";dbname=".$this->db, $this->user, $this->pass); // Make a MySQL conn w/ PDO
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); // So we can catch PDO errors
        } catch (\PDOException $e) {
            die("There is an error. Error: ".$e->getMessage()."\n".var_dump($config));
        }
    }

    public function getConnection(){
        return $this->conn;
    }

}