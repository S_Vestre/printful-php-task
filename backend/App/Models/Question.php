<?php
namespace App\Models;

use \App\BaseModel;
use \App\Models\Answer;

class Question extends BaseModel 
{

    protected $table = "Question";
    public $test_id;
    public $answers;
    private $db;

    public function __construct($db, $id = null)
    {
        parent::__construct($db);
        $this->db = $db;
        if(isset($id)) $this->test_id = $id;
    }

    /**
     * Get the question text
     *
     * @param Integer $id The question ID
     * @return String The text of the question
     */
    public function getQuestion($id) 
    {
        $query = "SELECT * FROM ".$this->table." WHERE id='".$id."'";
        $prp = $this->conn->prepare($query);
        $prp->execute();
        $row = $prp->fetch(\PDO::FETCH_ASSOC);
        return $row["name"];
    }

    /**
     * Get the first question of the test
     *
     * @param Integer $test_id If not passed, the objects property will be used
     * @return Integer The ID of the question
     */
    public function getFirstQuestion($test_id = null) 
    {
        $query = "SELECT id FROM Question WHERE test_id = '".($test_id ?: $this->test_id)."' ORDER BY id ASC LIMIT 0,1";
        $prp = $this->query($query);
        $row = $prp->fetch();
        return $row["id"];
    }

    /**
     * Get total number of questions for the specific test
     *
     * @param Integer $test_id If not passed, the objects property will be used
     * @return Integer The count
     */
    public function getTotalQuestions($test_id = null) 
    {
        $query = "SELECT COUNT(*) FROM Question WHERE test_id = '".($test_id ?: $this->test_id)."'";
        $prp = $this->query($query);
        $res = $prp->fetchColumn();
        return $res;
    }

    /**
     * Check if the passed test ID even exists
     *
     * @param Integer $test_id
     * @return Boolean
     */
    public function ifExists($test_id = null) 
    {
        $query = "SELECT COUNT(*) FROM Question WHERE test_id = '".($test_id ?: $this->test_id)."'";
        $prp = $this->query($query);
        $res = $prp->fetchColumn();
        if($res == 0 ) return false;
        return true;
    }
    
    /**
     * Get the next question from the current one within the Test
     *
     * @param Integer $current The current question ID
     * @param Integer $test_id The ID of the Test
     * @return Integer or Boolean if next isn't available
     */
    public function getNextQuestion($current, $test_id = null) 
    {
        $query = "SELECT * FROM Question WHERE id > '".$current."' AND test_id = '".($test_id ?: $this->test_id)."' ORDER BY id ASC LIMIT 0,1";
        $prp = $this->query($query);
        if($prp->rowCount() > 0){
            $row = $prp->fetch();
            return $row["id"];
        }
        return false;
    }

    /**
     * Passes an Answer object which you can access
     *
     * @param Integer $id The Question ID to access the Answers
     * @return Answer instance
     */
    public function Question($id) 
    {
        return new Answer($this->db, $id);
    }
    

}