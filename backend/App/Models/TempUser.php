<?php
namespace App\Models;

use \App\BaseModel;
use \App\Models\Test;
use \App\Models\UserAnswer;

class TempUser extends \App\BaseModel
{
    protected $table = "TempUser";

    public $id;
    public $userAnswer;
    private $testObj;

    public function __construct($db, $id = null) 
    {
        $this->testObj = new Test($db);
        parent::__construct($db);
        $this->id = $id;
        $this->userAnswer = new UserAnswer($db, $id);
    }

    /**
     * Create a temporary user with given name and test ID
     *
     * @param String $name Name of the user
     * @param Integer $test TestID of the chosen test
     * @return Array or false Returns array with newly added user data
     */
    public function createUser($name, $test) 
    {
        // If the given test ID doesnt exist in DB, return false
        if(!$this->testObj->questions->ifExists($test)) return false;
    
        $user = $this->new(
            "name, test_id, question_id, current_question, max_question, user_ip",
            "'".$name."', 
            '".$test."', 
            '".$this->testObj->questions->getFirstQuestion($test)."', 
            '1',
            '".$this->testObj->questions->getTotalQuestions($test)."',
            '".$_SERVER["REMOTE_ADDR"]."'"
        );
        if($user) return $user;
        return false;
    }

    /**
     * Find user Id by giving a name and testID
     *
     * @param String $name The name of the user
     * @param Integer $test_id The ID of the test the user is writing
     * @return Boolean
     */
    public function findUserId($name, $test_id) 
    {
        $query = "SELECT id FROM ".$this->table." WHERE name='".$name."' AND test_id='".$test_id."' AND user_ip='".$_SERVER["REMOTE_ADDR"]."' ORDER BY created_at DESC LIMIT 0,1";
        $prp = $this->query($query);
        if($prp->rowCount() > 0){
            $row = $prp->fetch(\PDO::FETCH_ASSOC);
            return $row["id"];
        } else return false;
    }

    /**
     * Sets the current question ID to the user
     *
     * @param Integer $new_id The new ID to set
     * @return Boolean
     */
    public function setQuestionId($new_id) 
    {
        $query = "UPDATE ".$this->table." SET question_id = '".$new_id."' WHERE id='".$this->id."'";
        $res = $this->query($query);
        if(!$res) return false;
        return true;
    }

    /**
     * Set the user field to finished, the user has completed the test
     *
     * @return Boolean
     */
    public function setFinished() 
    {
        $query = "UPDATE ".$this->table." SET finished = '1' WHERE id='".$this->id."'";
        $res = $this->query($query);
        if(!$res) return false;
        return true;
    }

    /**
     * Increments the users current question count by one
     *
     * @return Boolean
     */
    public function addToQuestion() 
    {
        $query = "UPDATE ".$this->table." SET current_question = current_question + 1 WHERE id='".$this->id."'";
        $res = $this->query($query);
        if(!$res) return false;
        return true;
    }
}