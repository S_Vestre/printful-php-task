<?php
namespace App\Models;

use \App\BaseModel;

class UserAnswer extends BaseModel {

    protected $table = "UserAnswer";

    private $userId;

    public function __construct($db, $id = null) {
        parent::__construct($db);
        $this->userId = $id;
    }

    /**
     * Submits the answer into the database
     *
     * @param Integer $test_id The ID of the Test the user is writing
     * @param Integer $answer_id The ID of the Answer the user answered
     * @param Integer $correct Whether the Answer was correct or not (1 or 0)
     * @param Integer $user_id The User's ID
     * @return Array or Boolean if failed
     */
    public function giveAnswer($test_id, $answer_id, $correct, $user_id = null) {
        $answer = $this->new(
            "temp_user_id, answer_id, test_id, correct", 
            "'".($user_id ?: $this->userId)."', '".$answer_id."', '".$test_id."', '".$correct."'"
        );
        return $answer;
    }
}