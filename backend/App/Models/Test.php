<?php
namespace App\Models;

use \App\BaseModel;
use \App\Models\Question;

class Test extends BaseModel {

    protected $table = "Test";
    public $id;
    public $questions;

    public function __construct($db, $id = null) {
        parent::__construct($db);
        $this->questions = new Question($db, $id);
    }

}