<?php
namespace App\Models;

use \App\BaseModel;

class Answer extends BaseModel 
{
    protected $table = "Answer";

    public $question_id;

    public function __construct($db, $id) 
    {
        parent::__construct($db);
        if(isset($id)) $this->question_id = $id;
    }

    /**
     * Get a list of answers in the specified question
     *
     * @param Integer $question_id The question ID whose Answers you want to get
     * @return Array 
     */
    public function getAnswers($question_id = null) 
    {
        $query = "SELECT * FROM ".$this->table." WHERE question_id='".($question_id ?: $this->question_id)."'";
        $prp = $this->query($query);
        $rows = [];
        while($row = $prp->fetch(\PDO::FETCH_ASSOC)) {
            $item = [
                'id' => $row["id"],
                'name' => $row["name"]
            ];
            array_push($rows, $item);
        }
        return $rows;
    }
    
    /**
     * Get the correct answer ID within the given question
     *
     * @param Integer $question_id The Question ID, if not given, object property will be used
     * @return Integer
     */
    public function getCorrectId($question_id = null) 
    {
        $query = "SELECT id FROM ".$this->table." WHERE question_id='".($question_id ?: $this->question_id)."' AND correct='1'";
        $prp = $this->query($query);
        $row = $prp->fetch(\PDO::FETCH_ASSOC);
        return $row["id"];
    }

    /**
     * Checks whether the given answer is correct
     *
     * @param Integer $id The Answer ID
     * @param Integer $question_id The Question ID [optional]
     * @return Boolean
     */
    public function isCorrect($id, $question_id = null) 
    {
       $res = $this->query("SELECT * FROM ".$this->table." WHERE question_id='".($question_id ?: $this->question_id)."' AND id='".$id."'");
       $row = $res->fetch(\PDO::FETCH_ASSOC);
       return $row["correct"];
    }
}