# Printful PHP Technical Task

## 1. Vue setup

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production in /dist folder
```
npm run build
```

## 2. Backend setup

#### Make the server public root in `/dist` which is compiled by running `npm run build`

#### Import the `PrintfulTest.sql` in your database.
#### `/backend/App` has a `Config.php` file, it contains MySQL Server credentials, change them accordingly
#### Run `composer install` to generate the autoload files
